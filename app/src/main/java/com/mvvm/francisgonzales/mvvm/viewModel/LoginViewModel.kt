package com.mvvm.francisgonzales.mvvm.viewModel

import android.arch.lifecycle.ViewModel
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import com.mvvm.francisgonzales.mvvm.Interface.LoginResultCallBacks
import com.mvvm.francisgonzales.mvvm.model.User

class LoginViewModel (private val listener:LoginResultCallBacks): ViewModel(){
    private val user:User

    init {
        this.user = User("","");
    }

    val emailTextWatcher:TextWatcher
        get() = object:TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                user.setEmail(s.toString())
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }
        }

    val passwordTextWatcher:TextWatcher
        get() = object:TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                user.setPassword(s.toString())
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                //TODO("not implemented") To change body of created functions use File | Settings | File Templates.
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
               // TODO("not implemented") To change body of created functions use File | Settings | File Templates.
            }
        }

    fun onLoginClicked(v: View){
        var loginCode:Int = user.isDataValid()

        if(loginCode == 0)
            listener.onSuccess("Email null")
        else if (loginCode == 1)
            listener.onSuccess("wrong email pattern")
        else if (loginCode == 2)
            listener.onError("password length")
        else
            listener.onSuccess("Login Success")
    }
}