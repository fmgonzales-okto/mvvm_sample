package com.mvvm.francisgonzales.mvvm.core

import android.annotation.TargetApi
import android.content.Context
import android.graphics.Point
import android.os.Build
import android.view.WindowManager
import com.mvvm.francisgonzales.mvvm.R.attr.height
import com.mvvm.francisgonzales.mvvm.R.attr.layout_constrainedWidth

class DensityAndScreenSizeHelper  {

     lateinit var context: Context

     fun DensityAndScreenSizeHelper(context: Context) {
        this.context = context
    }

    fun getDensityUsedByThisDevice(context: Context): String {
        return context.resources.displayMetrics.densityDpi.toString()
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1) // API 17
    fun getRealScreenResolution(): String {
        val wm: WindowManager = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val size = Point()

        if (wm != null) {
            wm.defaultDisplay.getRealSize(size)
        }
            val width = size.x
            val height = size.y

        return String.format("%sx%s", width, height)
    }
}