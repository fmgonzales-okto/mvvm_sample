package com.mvvm.francisgonzales.mvvm.view

import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.mvvm.francisgonzales.mvvm.Interface.LoginResultCallBacks

import com.mvvm.francisgonzales.mvvm.R
import com.mvvm.francisgonzales.mvvm.R.id.loginLayout
import com.mvvm.francisgonzales.mvvm.core.DensityAndScreenSizeHelper
import com.mvvm.francisgonzales.mvvm.viewModel.LoginViewModel
import com.mvvm.francisgonzales.mvvm.viewModel.LoginViewModelFactory
import com.mvvm.francisgonzales.mvvm.databinding.ActivityMainBinding
import kotlinx.android.synthetic.main.activity_main.*
import android.R.attr.y
import android.R.attr.x
import android.graphics.Point
import android.view.Display



class MainActivity : AppCompatActivity(), LoginResultCallBacks {

    override fun onSuccess(message: String) {
        Toast.makeText(this,message,Toast.LENGTH_SHORT).show()
    }

    override fun onError(message: String) {
        Toast.makeText(this,message,Toast.LENGTH_SHORT).show()
    }

    val densityAndScreenSizeHelper = DensityAndScreenSizeHelper()



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val display = windowManager.defaultDisplay
        val size = Point()
        display.getSize(size)
        val width = size.x
        val height = size.y

        val res = String.format("%sx%s", width, height)

//        if(res.equals("480x800")){
//            loginLayout.getLayoutParams().height = 500
//        }

        val activityMainBinding =  DataBindingUtil.setContentView<ActivityMainBinding>(this,R.layout.activity_main)
        activityMainBinding.viewModel = ViewModelProviders.of(this,LoginViewModelFactory(this))
                .get(LoginViewModel::class.java)
    }


}
