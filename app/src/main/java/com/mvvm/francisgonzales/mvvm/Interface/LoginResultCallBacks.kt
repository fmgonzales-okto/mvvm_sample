package com.mvvm.francisgonzales.mvvm.Interface

interface LoginResultCallBacks {
    fun onSuccess(message:String)
    fun onError(message:String)
}